# Скрипт создан для исключения версионирования файлов настроек
# Изначальные настройки копируются из версионных файлов. Дальнейшие действия не сбивают контроль версий.
import os
from datetime import datetime

import config
import user_forecasts

cwd = os.getcwd()

# data storage
os.mkdir('data')

# config
_config = {'allergens': [{'factor': 1, 'levels': [{'color': '#009900', 'from': 1, 'to': 10},
                                                  {'color': '#ffff00', 'from': 10, 'to': 100},
                                                  {'color': '#ff8c00', 'from': 100, 'to': 1000},
                                                  {'color': '#ff0000', 'from': 1000, 'to': 5000},
                                                  {'color': '#800080', 'from': 5000, 'to': 10000}], 'name': 'Ольха',
                          'ru_units': 'моль/куб.м', 'units': 'mole/m3', 'variable': 'cnc_POLLEN_ALDER_m22'},
                         {'factor': 1, 'levels': [{'color': '#009900', 'from': 1, 'to': 10},
                                                  {'color': '#ffff00', 'from': 10, 'to': 100},
                                                  {'color': '#ff8c00', 'from': 100, 'to': 1000},
                                                  {'color': '#ff0000', 'from': 1000, 'to': 5000},
                                                  {'color': '#800080', 'from': 5000, 'to': 10000}], 'name': 'Береза',
                          'ru_units': 'ед/куб.м', 'units': 'number/m3', 'variable': 'cnc_POLLEN_BIRCH_m22'},
                         {'factor': 1, 'levels': [{'color': '#009900', 'from': 1, 'to': 10},
                                                  {'color': '#ffff00', 'from': 10, 'to': 100},
                                                  {'color': '#ff8c00', 'from': 100, 'to': 1000},
                                                  {'color': '#ff0000', 'from': 1000, 'to': 5000},
                                                  {'color': '#800080', 'from': 5000, 'to': 10000}], 'name': 'Злаки',
                          'ru_units': 'моль/куб.м', 'units': 'mole/m3', 'variable': 'cnc_POLLEN_GRASS_m32'},
                         {'factor': 1, 'levels': [{'color': '#009900', 'from': 1, 'to': 10},
                                                  {'color': '#ffff00', 'from': 10, 'to': 100},
                                                  {'color': '#ff8c00', 'from': 100, 'to': 1000},
                                                  {'color': '#ff0000', 'from': 1000, 'to': 5000},
                                                  {'color': '#800080', 'from': 5000, 'to': 10000}], 'name': 'Полынь',
                          'ru_units': 'моль/куб.м', 'units': 'mole/m3', 'variable': 'cnc_POLLEN_MUGWORT_m18'},
                         {'factor': 1, 'levels': [{'color': '#009900', 'from': 1, 'to': 10},
                                                  {'color': '#ffff00', 'from': 10, 'to': 100},
                                                  {'color': '#ff8c00', 'from': 100, 'to': 1000},
                                                  {'color': '#ff0000', 'from': 1000, 'to': 5000},
                                                  {'color': '#800080', 'from': 5000, 'to': 10000}], 'name': 'Олива',
                          'ru_units': 'моль/куб.м', 'units': 'mole/m3', 'variable': 'cnc_POLLEN_OLIVE_m28'},
                         {'factor': 1, 'levels': [{'color': '#009900', 'from': 1, 'to': 10},
                                                  {'color': '#ffff00', 'from': 10, 'to': 100},
                                                  {'color': '#ff8c00', 'from': 100, 'to': 1000},
                                                  {'color': '#ff0000', 'from': 1000, 'to': 5000},
                                                  {'color': '#800080', 'from': 5000, 'to': 10000}], 'name': 'Амброзия',
                          'ru_units': 'моль/куб.м', 'units': 'mole/m3', 'variable': 'cnc_POLLEN_RAGWEED_m18'}],
           'source': {'variables': [{'key': 'url_root',
                                     'value': 'http://silam.fmi.fi/thredds/fileServer/silam_europe_pollen_v5_6_1-RU/files'},
                                    {'key': 'file_format', 'value': 'SILAM-POLLEN-europe-RU_v5_6_1_%Y%m%d00.nc4'},
                                    {'key': 'postfix', 'value': 'email=yapetus@yandex.ru'}],
                      'template': '{url_root}/{file_format}?{postfix}'},
           'zones': [{'distance': 0.8, 'lat': 55.752, 'lon': 37.616, 'name': 'Москва'}],
           'common': {'active': True,
                      'cache_path': os.path.join(cwd, 'static', 'forecasts'),
                      'chunk_size': 128,
                      'root': '/srv/flask-uwsgi/silam-pollen-club-api',
                      'storage_path': os.path.join(cwd, 'data')}}

config.save_allergens(_config['allergens'])
config.save_common(_config['common'])
config.save_source(_config['source'])
config.save_zones(_config['zones'])

# user_forecasts
today = datetime.today()
_user_forecasts = [{'allergen': allergen_name,
                    'date': today.strftime('%Y-%m-%d'),
                    'value': '0',
                    'zone': 'Москва'}
                   for allergen_name in (a['name'] for a in _config['allergens'])]
user_forecasts.save_user_forecasts(_user_forecasts)
