# нативные модули
from datetime import datetime, timedelta
import os
import json

# модули, требующие установки дополнительных пакетов
import requests
import numpy as np
from netCDF4 import Dataset

# модули проекта
from config import config
import status
from common import transform, make_overlay

# специфичный импорт
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt


def download_netcdf(dl_file=None):
    variables = dict((var['key'], var['value']) for var in config['source']['variables'])
    # Если файл не указан
    if dl_file is None:
        # print('DOWNLOAD FILE NOT SET')
        dl_file = datetime.utcnow().strftime(variables['file_format'])
        # print('SETTING %s' % dl_file)
    # Получение статуса файла
    file_dict = status.get(dl_file)
    # print('FILE STATUS: %s' % file_dict['status'])
    # Если файл имеет статус "отсутствует"
    if file_dict['status'] == 'absent':
        try:
            url = config['source']['template']
            url = url.replace('{file_format}', dl_file)
            for key in variables:
                if '{%s}' % key in url:
                    url = url.replace('{%s}' % key, variables[key])
            # print('TRYING TO DOWNLOAD...')
            r = requests.get(url, stream=True)
            # print('HTTP STATUS CODE: %s' % r.status_code)
            if r.status_code == 200:
                # Смена статуса на "скачивается"
                status.update(dl_file, 'downloading')
                # Запись потока в локальное хранилище
                with open(os.path.join(config['common']['storage_path'], dl_file), 'wb') as f:
                    for chunk in r.iter_content(int(config['common']['chunk_size'])):
                        f.write(chunk)
                # Смена статуса на "скачался"
                status.update(dl_file, 'downloaded')
                return True, 'success', 'download complete'
            else:
                status.update(dl_file, 'absent')
                return False, 'download status code', str(r.status_code)
        except Exception as e:
            return False, 'exception', str(e)
    else:
        return False, 'file status: %s' % file_dict['status'], dl_file


def make_geojson(netcdf=None):
    """
    Обрабатывает файл netCDF4, результатом чего получается набор GeoJSON файлов.
    Структура netCDF4 следующая:
    переменная аллергена -> времянная отметка -> высотная отметка -> индекс широты -> индекс долготы -> концентрация
    :param netcdf_file_list:
    :return: ---
    """
    if netcdf is None:
        netcdf_status_list = status.with_('downloaded')
        if netcdf_status_list:
            netcdf_file_list = [ns['filename'] for ns in netcdf_status_list]
        else:
            netcdf_file_list = []
    else:
        netcdf_file_list = [netcdf]
    for netcdf_filename in netcdf_file_list:
        print('PROCESSING', netcdf_filename)
        status.update(netcdf_filename, 'processing')
        variables = dict((var['key'], var['value']) for var in config['source']['variables'])
        allergens_count = len(config['allergens'])
        variable_names = [a['variable'] for a in config['allergens']]
        allergen_names = [a['name'] for a in config['allergens']]
        allergen_levels = [a['levels'] for a in config['allergens']]
        data = Dataset(os.path.join(config['common']['storage_path'], netcdf_filename), 'r', format='NETCDF4')
        
        # Определение временных интервалов
        root_time = datetime.strptime(netcdf_filename, variables[u'file_format'])
        times = data.variables['time'][:]
        
        # Извлечение из набора данных сведений о координатной сети
        r_lons = [round(float(l), 5) for l in data.variables['rlon']]
        r_lats = [round(float(l), 5) for l in data.variables['rlat']]
        # Расширение области покрытия на один шаг в 4 направлениях (для получения замкнутых полигонов)
        for l in (r_lons, r_lats):
            l.insert(0, l[0] * 2 - l[1])
            l.append(l[-1] * 2 - l[-2])
        # Проход по каждой используемой переменной
        for i in range(allergens_count):
            variable_name = variable_names[i]
            allergen = list(filter(lambda x: x['variable'] == variable_name, config['allergens']))[0]
            factor = allergen['factor']
            level_set = allergen_levels[i]
            allergen_name = allergen_names[i]
            # SILAM использует только одну высотную отметку (и она указана прямо в имени переменной),
            # т.е. ниже исключен одноразовый цикл с итерируемой переменной h
            # for h in range(len(data.variables['height'])):
            h = 0
            for t in range(len(data.variables['time'])):
                field_data = data.variables[variable_name][t][h] * factor
                
                # Создание поля с нулевыми границами (для получения замкнутых полигонов)
                n_field_data = np.zeros((len(r_lats), len(r_lons)))
                n_field_data[1:len(r_lats) - 1, 1:len(r_lons) - 1] = field_data
                
                forecast_time = root_time + timedelta(0, int(times[t]))
                
                # Наложение пользовательских значений
                overlay_data = make_overlay(r_lats, r_lons, allergen_name, forecast_time.strftime('%Y-%m-%d'))
                n_field_data = np.maximum(n_field_data, overlay_data)
                
                # polygons = []  # Google Maps
                ll_polygons = []  # Leaflet
                
                for level_range in level_set:
                    contour_set = plt.contour(r_lons, r_lats, n_field_data, [level_range['from'], level_range['to']])
                    # polygon = []  # Google Maps
                    outer_ring = []  # Leaflet
                    inner_ring = []  # Leaflet
                    if contour_set.layers[0] == level_range['from']:
                        for line in contour_set.collections[0].get_paths():
                            if len(line.vertices) > 2:
                                # polygon_fragment = []  # Google Maps
                                ll_polygon_fragment = []  # Leaflet
                                points = [(round(pt[0], 5), round(pt[1], 5)) for pt in line.vertices]
                                for lon, lat in points:
                                    longitude, latitude = transform(lon, lat)
                                    # polygon_fragment.append(dict(lng=longitude, lat=latitude))  # Google Maps
                                    ll_polygon_fragment.append([latitude, longitude])  # Leaflet
                                # polygon.append(polygon_fragment)  # Google Maps
                                outer_ring.append(ll_polygon_fragment)  # Leaflet
                    # Добавление пустот
                    if len(contour_set.layers) > 1 and contour_set.layers[1] == level_range['to']:
                        for line in contour_set.collections[1].get_paths():
                            if len(line.vertices) > 2:
                                polygon_fragment = []
                                ll_polygon_fragment = []  # Leaflet
                                points = [(round(pt[0], 5), round(pt[1], 5)) for pt in line.vertices[-1::-1]]
                                for lon, lat in points:
                                    longitude, latitude = transform(lon, lat)
                                    polygon_fragment.append(dict(lng=longitude, lat=latitude))
                                    ll_polygon_fragment.append([latitude, longitude])  # Leaflet
                                # polygon.append(polygon_fragment)  # Google Maps
                                inner_ring.append(ll_polygon_fragment)  # Leaflet
                    # polygons.append(dict(paths=polygon, geodesic=True,
                    #                      strokeColor=level_range['color'], strokeOpacity=1.0, strokeWeight=1,
                    #                      fillColor=level_range['color'], fillOpacity=.5))  # Google Maps
                    ll_polygons.append(dict(latlngs=[outer_ring, inner_ring], color=level_range['color'], opacity=1.0,
                                            weight=1, fillColor=level_range['color'], fillOpacity=.5))  # Leaflet
                
                # Дамп результата в json
                cache_dir = config['common']['cache_path']
                year = str(forecast_time.year)
                month = str(forecast_time.month)
                day = str(forecast_time.day)
                hour = str(forecast_time.hour)
                os.makedirs(os.path.join(cache_dir, allergen_name, year, month, day), exist_ok=True)
                # with open(os.path.join(cache_dir, allergen_name, year, month, day, '%s.json' % hour), 'w') as f:
                #     json.dump(polygons, f)  # Google Maps
                with open(os.path.join(cache_dir, allergen_name, year, month, day,
                                       '%s.json' % hour), 'w') as f:
                    json.dump(ll_polygons, f)  # Leaflet
        status.update(netcdf_filename, 'processed')


def clean_deprecated():
    pass
