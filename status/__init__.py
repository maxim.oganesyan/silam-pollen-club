import os
from yaml import load, dump
from datetime import datetime
from config import common

possible_statuses = '''
"absent"
"downloading"
"downloaded"
"processing"
"processed"
"cached"
'''

dir_path = os.path.dirname(os.path.realpath(__file__))
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def get_source_files():
    try:
        with open(os.path.join(dir_path, 'source_files.yaml'), 'r', encoding='utf-8') as stream:
            source_files = load(stream, Loader=Loader) or list()
    except FileNotFoundError:
        source_files = list()
    return source_files


def dump_source_files(source_files):
    with open(os.path.join(dir_path, 'source_files.yaml'), 'w', encoding='utf-8') as stream:
        dump(source_files, stream, Dumper=Dumper)


def get(filename):
    source_files = get_source_files()
    filtered = list(filter(lambda x: x['filename'] == filename, source_files))
    if filtered:
        return filtered[0]  # Возможен только один файл
    else:
        file_dict = dict(filename=filename, status='absent', date=datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'))
        source_files.append(file_dict)
        dump_source_files(source_files)
        return file_dict


def with_(status):
    source_files = get_source_files()
    filtered = list(filter(lambda x: x['status'] == status, source_files))
    return filtered


def update(filename, status):
    source_files = get_source_files()
    filtered = list(filter(lambda x: x['filename'] == filename, source_files))
    if filtered:
        file_dict = filtered[0]  # Возможен только один файл
    else:
        file_dict = dict(filename=filename, status='absent', date=datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'))
        source_files.append(file_dict)
    file_dict['status'] = status
    file_dict['date'] = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    dump_source_files(source_files)
    return None


def delete(filename):
    source_files = get_source_files()
    filtered = list(filter(lambda x: x['filename'] == filename, source_files))
    os.remove(os.path.join(common['storage_path'], filtered[0]))
    source_files.remove(filtered[0])
    dump_source_files(source_files)
    return None
