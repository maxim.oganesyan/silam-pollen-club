"""
Процедуры и функции собираются здесь до распределения по необходимым модулям.
Если необходимого модуля нет, то здесь и остаются.
"""
# нативные модули
from typing import Tuple, Optional
from math import radians as rad, degrees as deg, cos, sin, atan2, asin, sqrt
from datetime import datetime

# модули, требующие установки дополнительных пакетов
import numpy as np

# модули проекта
from config import config
from user_forecasts import user_forecasts


def generate_template(variables: list, template: str) -> str:
    now = datetime.utcnow()
    new_template = template
    for variable in variables:
        new_template = new_template.replace('{%s}' % variable['key'], variable['value'])
    new_template = datetime.strftime(now, new_template)
    return new_template


def transform(i_lon: float, i_lat: float,
              pole_lon: float = 0, pole_lat: float = -30, backward: bool = True) -> Tuple[float, float]:
    """
    Функция трансформации исходных координат в географические.
    :param i_lon: исходная долгота
    :param i_lat: исходная широта
    :param pole_lon: полярная долгота исходной сети
    :param pole_lat: полярная широта исходной сети
    :param backward: обратное преобразования
    :return: пара, представляющая собой трансформировынные широту и долготу
    """
    lon = rad(i_lon)
    lat = rad(i_lat)
    # Перевод системы координат из сферической в декартову
    x = cos(lon) * cos(lat)
    y = sin(lon) * cos(lat)
    z = sin(lat)
    
    if backward:
        theta = -rad(90 + pole_lat)
        phi = -rad(pole_lon)
        
        x_new = cos(theta) * cos(phi) * x + sin(phi) * y + sin(theta) * cos(phi) * z
        y_new = -cos(theta) * sin(phi) * x + cos(phi) * y - sin(theta) * sin(phi) * z
        z_new = -sin(theta) * x + cos(theta) * z
    else:
        theta = rad(90 + pole_lat)
        phi = rad(pole_lon)
        x_new = cos(theta) * cos(phi) * x + cos(theta) * sin(phi) * y + sin(theta) * z
        y_new = -sin(phi) * x + cos(phi) * y
        z_new = -sin(theta) * cos(phi) * x - sin(theta) * sin(phi) * y + cos(theta) * z
    
    lon_new = deg(atan2(y_new, x_new))
    lat_new = deg(asin(z_new))
    
    return lon_new, lat_new


def make_overlay(lats: list, lons: list, allergen: str, date: str) -> np.ndarray:
    custom_field = np.zeros((len(lats), len(lons)))
    forecasts = list(filter(lambda x: x['allergen'] == allergen and x['date'] == date, user_forecasts))
    for forecast in forecasts:
        zone = list(filter(lambda x: x['name'] == forecast['zone'], config['zones']))
        value = float(forecast['value'])
        if not value % 1.0:
            value += .01  # Для случаев, когда границы проходят прямо по узлу
        if zone:
            zone = zone[0]
            z_lon, z_lat = transform(zone['lon'], zone['lat'], backward=False)
            for i in range(len(lats)):
                for j in range(len(lons)):
                    lon, lat = lons[j], lats[i]
                    if (z_lon - lon) ** 2 + (z_lat - lat) ** 2 <= zone['distance'] ** 2:
                        custom_field[i][j] = value
    return custom_field
