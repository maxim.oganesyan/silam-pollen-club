import os
from yaml import load, dump

dir_path = os.path.dirname(os.path.realpath(__file__))
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

try:
    with open(os.path.join(dir_path, 'user_forecasts.yaml'), 'r', encoding='utf-8') as stream:
        user_forecasts = load(stream, Loader=Loader)
except FileNotFoundError:
    user_forecasts = list()

def save_user_forecasts(forecasts):
    user_forecasts = forecasts
    with open(os.path.join(dir_path, 'user_forecasts.yaml'), 'w', encoding='utf-8') as stream:
        dump(user_forecasts, stream, Dumper=Dumper, allow_unicode=True)
    return user_forecasts



