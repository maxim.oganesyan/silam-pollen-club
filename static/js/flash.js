const flash = (message, status = 'ok') => {
    const uNumber = `${(+new Date).toString(16)}`;
    $('#flash').append('<div id="message-f' + uNumber + '" class="' + status + '">' + message + '</div>');
    setTimeout(() => {
        $('#message-f' + uNumber).remove()
    }, 5000);
};