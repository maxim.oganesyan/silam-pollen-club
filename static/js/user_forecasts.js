$(document).ready(() => {
    let updateForecastEvents = (jQueryObject) => {
        $(jQueryObject.find('.forecast-remove')[0]).on('click', () => {
            jQueryObject.remove();
        });
    };

    $('.forecast').each((index, item) => {
        updateForecastEvents($(item));
    });

    let addForecastButton = $($('#forecasts button.add-forecast')[0]);
    addForecastButton.on('click', () => {
        let parentRow = $(addForecastButton.parents('.row')[0]);
        let templateHtml = $(parentRow.children('.template')[0]).html();
        parentRow.before(templateHtml);
        let newRow = $(parentRow.parent().children('.forecast').last()[0]);
        updateForecastEvents(newRow);
    });


    $('#forecasts-actions-save').on('click', () => {
        let forecasts = [];
        $('.forecast:not(.template .forecast)').each((index, item) => {
            let forecast = {};
            let forecastContainer = $(item);
            forecast['zone'] = $(forecastContainer.find('select[name="zone"] option:selected')[0]).val();
            forecast['date'] = $(forecastContainer.find('input[name="date"]')[0]).val();
            forecast['allergen'] = $(forecastContainer.find('select[name="allergen"] option:selected')[0]).val();
            forecast['value'] = $(forecastContainer.find('input[name="value"]')[0]).val();
            forecasts.push(forecast);
        });
        let myJSON = JSON.stringify(forecasts);
        $.ajax({
            url: updatePath,
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: myJSON,
            processData: false,
            success: function (data, textStatus, jQxhr) {
                flash(data.message, data.status);
                // console.log(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                flash(errorThrown, textStatus);
                console.log(errorThrown);
            }
        });
    });
});
