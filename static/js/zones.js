$(document).ready(() => {
    let updateZoneEvents = (jQueryObject) => {
        $(jQueryObject.find('.zone-remove')[0]).on('click', () => {
            jQueryObject.remove();
        });
    };
    let addZoneButton = $($('#zones button.add-zone')[0]);

    $('.zone:not(.template .zone)').each((index, item) => {
        updateZoneEvents($(item));
    });

    addZoneButton.on('click', () => {
        let parentRow = $(addZoneButton.parents('.row')[0]);
        let templateHtml = $(parentRow.children('.template')[0]).html();
        parentRow.before(templateHtml);
        let newRow = $(parentRow.parent().children('.zone').last()[0]);
        updateZoneEvents(newRow);
    });


    $('#zones-actions-save').on('click', () => {
        let zones = [];

        $('.zone:not(.template .zone)').each((index, item) => {
            let zone = {};
            let zoneContainer = $(item);
            zone['name'] = $(zoneContainer.find('input[name="name"]')[0]).val();
            zone['lat'] = $(zoneContainer.find('input[name="lat"]')[0]).val();
            zone['lon'] = $(zoneContainer.find('input[name="lon"]')[0]).val();
            zone['distance'] = $(zoneContainer.find('input[name="distance"]')[0]).val();
            zones.push(zone);
        });

        let myJSON = JSON.stringify(zones);

        $.ajax({
            url: updatePath,
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: myJSON,
            processData: false,
            success: function (data, textStatus, jQxhr) {
                flash(data.message, data.status);
                // console.log(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                flash(errorThrown, textStatus);
                console.log(errorThrown);
            }
        });
    });
});
