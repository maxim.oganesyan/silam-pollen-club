$(document).ready(() => {
    let updateVariableEvents = (jQueryObject) => {
        $(jQueryObject.find('.variable-remove')[0]).on('click', () => {
            jQueryObject.remove();
        });
    };

    $('.row.variable').each((index, item) => {
        updateVariableEvents($(item));
    });

    let addVariableButton = $($('#variables button.add-variable')[0]);
    addVariableButton.on('click', () => {
        let parentRow = $(addVariableButton.parents('.row')[0]);
        let templateHtml = $(parentRow.children('.template')[0]).html();
        parentRow.before(templateHtml);
        let newRow = $(parentRow.parent().children('.variable').last()[0]);
        updateVariableEvents(newRow);
    });

    let jsonifyForms = () => {
        let variables = [];
        $('.row.variable:not(.template .row.variable)').each((index, item) => {
            let variableRow = $(item);
            let variable = {};
            variable['key'] = $(variableRow.find('input[name="key"]')[0]).val();
            variable['value'] = $(variableRow.find('input[name="value"]')[0]).val();
            variables.push(variable);
        });
        let template = $($('input[name="template"]')[0]).val();
        let source = {variables: variables, template: template};
        return JSON.stringify(source);
    };

    $('#check-template').on('click', () => {
        let templateResult = $('#template-result');
        let myJSON = jsonifyForms();
        $.ajax({
            url: checkTemplatePath,
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: myJSON,
            processData: false,
            success: function (data, textStatus, jQxhr) {
                flash(data.message, data.status);
                // console.log(data);
                templateResult.attr('href', data.message);
                templateResult.text(data.message);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                flash(errorThrown, textStatus);
                console.log(errorThrown);
            }
        });
    });

    $('#source-actions-save').on('click', () => {
        let myJSON = jsonifyForms();
        $.ajax({
            url: updateSourcePath,
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: myJSON,
            processData: false,
            success: function (data, textStatus, jQxhr) {
                flash(data.message, data.status);
                // console.log(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                flash(errorThrown, textStatus);
                console.log(errorThrown);
            }
        });
    });
});
