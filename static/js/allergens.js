$(document).ready(() => {
    let updateLevelEvents = (jQueryObject) => {
        let colorPick = $(jQueryObject.find('.level-color-pick')[0]);
        let colorText = $(jQueryObject.find('.level-color-text')[0]);
        colorPick.on('change', () => {
            colorText.val(colorPick.val())
        });
        colorText.on('change', () => {
            colorPick.val(colorText.val())
        });
        $(jQueryObject.find('.level-remove')[0]).on('click', () => {
            jQueryObject.remove();
        });
    };

    $('.row.level').each((index, item) => {
        updateLevelEvents($(item));
    });

    $('.allergen-levels button.add-template').each((index, item) => {
        let button = $(item);
        button.on('click', () => {
            let parentRow = $(button.parents('.row')[0]);
            let templateHtml = $(parentRow.children('.template')[0]).html();
            parentRow.before(templateHtml);
            let newRow = $(parentRow.parent().children('.level').last()[0]);
            updateLevelEvents(newRow);
        });
    });

    $('#allergens-actions-save').on('click', () => {
        let allergens = [];
        $('.allergen').each((index, item) => {
            let allergen = {};
            let levels = [];
            let allergenContainer = $(item);
            allergen['name'] = $(allergenContainer.find('input[name="name"]')[0]).val();
            allergen['variable'] = $(allergenContainer.find('input[name="variable"]')[0]).val();
            allergen['units'] = $(allergenContainer.find('input[name="units"]')[0]).val();
            allergen['ru_units'] = $(allergenContainer.find('input[name="ru_units"]')[0]).val();
            allergen['factor'] = parseFloat($(allergenContainer.find('input[name="factor"]')[0]).val());
            allergenContainer.find('.allergen-levels .level:not(.template .level)').each((index, item) => {
                let level = {};
                let levelContainer = $(item);
                level['from'] = parseFloat($(levelContainer.find('input[name="from"]')[0]).val());
                level['to'] = parseFloat($(levelContainer.find('input[name="to"]')[0]).val());
                level['color'] = $(levelContainer.find('input[name="color-text"]')[0]).val();
                levels.push(level);
            });
            allergen['levels'] = levels;
            allergens.push(allergen);
        });
        let myJSON = JSON.stringify(allergens);
        $.ajax({
            url: updatePath,
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: myJSON,
            processData: false,
            success: function (data, textStatus, jQxhr) {
                flash(data.message, data.status);
                // console.log(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                flash(errorThrown, textStatus);
                console.log(errorThrown);
            }
        });
    });
});
