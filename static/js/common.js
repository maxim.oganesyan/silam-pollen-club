$(document).ready(() => {
    $('#common-actions-save').on('click', () => {
        let params = {};
        $('input[type="text"]').each((index, item) => {
            let input = $(item);
            params[input.attr('name')] = input.val();
        });
        $('input[type="number"]').each((index, item) => {
            let input = $(item);
            params[input.attr('name')] = parseFloat(input.val());
        });
        $('input[type="radio"]:checked').each((index, item) => {
            let input = $(item);
            params[input.attr('name')] = (input.val() === 'true');
        });
        let myJSON = JSON.stringify(params);
        $.ajax({
            url: updateCommonPath,
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: myJSON,
            processData: false,
            success: function (data, textStatus, jQxhr) {
                flash(data.message, data.status);
                // console.log(data);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                flash(errorThrown, textStatus);
                console.log(errorThrown);
            }
        });
    });
});