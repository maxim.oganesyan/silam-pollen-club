let apiUrl = 'https://api.pollen.club';
$.ajax(
    {
        url: apiUrl + '/static/legend.json',
        dataType: 'json',
        success: (allergens) => {
            let container = $($('div.legend-container')[0]);
            let tableIndex = 0;
            $.each(allergens, (index, item) => {
                container.append('<div class="col-6 col-sm-4 col-lg">' +
                    '<h2>' + item['name'] + '</h2>\n' +
                    '<div>' + item['units'] + '</div>\n' +
                    '<table></table>\n' +
                    '</div>');
                let table = $(container.find('table')[tableIndex]);
                $.each(item['levels'], (i, level) => {
                    table.append('<tr style="background-color: ' + level['color'] + ';">\n' +
                        '<td>' + level['range'] + '</td>\n' +
                        '</tr>\n');
                });
                tableIndex += 1
            });
        }
    }
);