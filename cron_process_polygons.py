import os
from file_processing import make_geojson

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path)
make_geojson()
