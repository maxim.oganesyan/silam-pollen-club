# нативные модули
from datetime import datetime, timedelta
import math, os, json

# модули, требующие установки дополнительных пакетов
from flask import Flask, request, jsonify, redirect, url_for, abort
from flask_mako import MakoTemplates, render_template
from flask_login import LoginManager, login_required, logout_user, UserMixin, current_user, login_user

# модули проекта
from config import config, save_allergens, save_source, save_common, save_zones
from common import generate_template, transform
from user_forecasts import user_forecasts as _user_forecasts, save_user_forecasts

app = Flask(__name__)
app.secret_key = 'SECRET'  # Changed for security reasons
mako = MakoTemplates(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
user_forecasts = _user_forecasts


class Admin(UserMixin):
    @property
    def id(self):
        return 0  # Changed for security reasons


def check_user(name, password):
    return False  # Changed for security reasons


admin = Admin()


@login_manager.user_loader
def load_user(id):
    if id == '100200300':
        return admin


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    elif request.method == 'POST':
        form = request.form
        name = form['name']
        password = form['password']
        if check_user(name, password):
            login_user(name, remember=True)

            return redirect(url_for('index'))
    else:
        return render_template('login.html', page='Вход в систему')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/')
@app.route('/index')
@login_required
def index():
    return render_template('index.html', page='Главная')


# Источник данных
@app.route('/config/source')
@login_required
def config_source():
    source = config['source']
    template_result = generate_template(source['variables'], source['template'])
    return render_template('source.html', page='Настройка источника', source=source, template_result=template_result)


@app.route('/config/source/check_template', methods=['POST'])
@login_required
def check_template():
    if request.is_json:
        data = request.json
        new_template = generate_template(data['variables'], data['template'])
        return jsonify(dict(status='ok', message=new_template))


# Аллергены
@app.route('/config/source/update', methods=['POST'])
@login_required
def update_source():
    if request.is_json:
        source = request.json
        save_source(source)
        return jsonify(dict(status='ok', message='saved'))


@app.route('/config/allergens')
@login_required
def config_allergens():
    allergens = config['allergens']
    return render_template('allergens.html', page='Настройка аллергенов', allergens=allergens)


@app.route('/config/allergens/update', methods=['POST'])
@login_required
def update_allergens():
    if request.is_json:
        allergens = request.json
        save_allergens(allergens)
        return jsonify(dict(status='ok', message='saved'))


@app.route('/config/allergens/update_legend')
@login_required
def update_legend():
    allergens = config['allergens']
    result_list = []
    for allergen in allergens:  # TODO: сортировка по приоритету
        allergen_dict = dict()
        result_list.append(allergen_dict)
        allergen_dict['units'] = allergen['ru_units']
        allergen_dict['name'] = allergen['name']
        allergen_dict['levels'] = list()
        for level in allergen['levels'][:-1]:
            level_dict = dict()
            level_dict['range'] = '%s - %s' % (level['from'], level['to'])
            level_dict['color'] = level['color']
            allergen_dict['levels'].append(level_dict)
        level = allergen['levels'][-1]
        level_dict = dict()
        level_dict['range'] = '> %s' % level['from']
        level_dict['color'] = level['color']
        allergen_dict['levels'].append(level_dict)
    with open(os.path.join(config['common']['root'], 'static', 'legend.json'), 'w', encoding='utf-8') as stream:
        json.dump(result_list, stream)
    return jsonify(dict(status='ok', message='saved'))


# Зоны
@app.route('/config/zones')
@login_required
def config_zones():
    zones = config['zones']
    return render_template('zones.html', page='Настройка зон', zones=zones)


@app.route('/config/zones/update', methods=['POST'])
@login_required
def update_zones():
    if request.is_json:
        zones = request.json
        for zone in zones:
            for key in ('lat', 'lon', 'distance'):
                zone[key] = float(zone[key])
        save_zones(zones)
        return jsonify(dict(status='ok', message='saved'))


# Прочее
@app.route('/config/common')
@login_required
def config_common():
    common = config['common']
    return render_template('common.html', page='Прочие настройки', common=common)


@app.route('/config/common/update', methods=['POST'])
def update_common():
    if request.is_json:
        common = request.json
        save_common(common)
        return jsonify(dict(status='ok', message='saved'))


# Карта
@app.route('/map')
@login_required
def map_page():
    return render_template('map.html', page='Карта')


@app.route('/user_forecasts')
@login_required
def edit_user_forecasts():
    return render_template('user_forecasts.html', page='Редактор прогнозов', user_forecasts=user_forecasts,
                           allergens=config['allergens'], zones=config['zones'])


@app.route('/update_user_forecasts', methods=['POST'])
@login_required
def update_user_forecasts():
    if request.is_json:
        global user_forecasts
        uf = request.json
        user_forecasts = save_user_forecasts(uf)
        return jsonify(dict(status='ok', message='saved'))


@app.route('/status_silam')
@login_required
def status_silam():
    import status  # Идёт обращение к файлу, с которым вне системы также работает cron
    status_list = status.get_source_files()
    return render_template('status_list.html', page='Статус SILAM', status_list=status_list)


# AJAX-функции
@app.route('/ajax/zones')
@login_required
def get_zones():
    step = math.pi / 24
    zones = []
    for zone in config['zones']:
        center_lon, center_lat = transform(zone['lon'], zone['lat'], backward=False)
        distance = zone['distance']
        latlngs = []
        for i in range(int(math.pi * 2 / step)):
            lng, lat = transform(math.sin(step * i) * distance + center_lon, math.cos(step * i) * distance + center_lat)
            latlngs.append((lat, lng))
        zones.append(dict(latlngs=latlngs, fillColor='#ff0000', color='#000000', popup='zone: %s' % zone['name']))
    return jsonify(zones)


@app.route('/ajax/network')
@login_required
def get_network():
    r_lons = list(i / 10 for i in range(101, 342))
    r_lats = list(i / 10 for i in range(-80, 160))
    latlngs = []
    for r_lon in r_lons:
        line = []
        for r_lat in r_lats:
            lng, lat = transform(r_lon, r_lat)
            line.append((lat, lng))
        latlngs.append(line)
    for r_lat in r_lats:
        line = []
        for r_lon in r_lons:
            lng, lat = transform(r_lon, r_lat)
            line.append((lat, lng))
        latlngs.append(line)
    network = dict(latlngs=latlngs, color='#000000', popup='network')
    return jsonify(network)


@app.route('/ajax/get_forecasts')
def get_forecasts():
    now = datetime.utcnow()
    allergens = [a['name'] for a in config['allergens']]
    zero_point = datetime(now.year, now.month, now.day, now.hour)
    root_path = config['common']['cache_path'].replace(config['common']['root'], '')
    interval_path = dict()
    for i in range(-72, 97):
        delta_point = zero_point + timedelta(seconds=3600 * i)
        year = str(delta_point.year)
        month = str(delta_point.month)
        day = str(delta_point.day)
        hour = str(delta_point.hour)
        # TODO: Проверка директорий
        interval_path[i] = '/%(year)s/%(month)s/%(day)s/%(hour)s.json' % dict(year=year, month=month,
                                                                              day=day, hour=hour)
    keys = list(interval_path.keys())
    interval = [min(keys), max(keys), 1]
    return jsonify(dict(root=root_path,
                        allergens=allergens,
                        interval=interval,
                        intervalPath=interval_path))


if __name__ == '__main__':
    app.run()
