import os
from file_processing import download_netcdf

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path)
download_netcdf()
