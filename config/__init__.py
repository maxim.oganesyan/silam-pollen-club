import os
from yaml import load, dump

dir_path = os.path.dirname(os.path.realpath(__file__))
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

try:
    with open(os.path.join(dir_path, 'allergens.yaml'), 'r', encoding='utf-8') as stream:
        allergens = load(stream, Loader=Loader)
except FileNotFoundError:
    allergens = []

try:
    with open(os.path.join(dir_path, 'source.yaml'), 'r', encoding='utf-8') as stream:
        source = load(stream, Loader=Loader)
except FileNotFoundError:
    source = dict()

try:
    with open(os.path.join(dir_path, 'zones.yaml'), 'r', encoding='utf-8') as stream:
        zones = load(stream, Loader=Loader)
except FileNotFoundError:
    zones = []

try:
    with open(os.path.join(dir_path, 'common.yaml'), 'r', encoding='utf-8') as stream:
        common = load(stream, Loader=Loader)
except:
    common = dict()

config = dict(allergens=allergens, source=source, zones=zones, common=common)


def save_allergens(_allergens):
    config['allergens'] = _allergens
    with open(os.path.join(dir_path, 'allergens.yaml'), 'w', encoding='utf-8') as stream:
        dump(_allergens, stream, Dumper=Dumper, allow_unicode=True)


def save_source(source):
    config['source'] = source
    with open(os.path.join(dir_path, 'source.yaml'), 'w', encoding='utf-8') as stream:
        dump(source, stream, Dumper=Dumper, allow_unicode=True)


def save_common(common):
    config['common'] = common
    with open(os.path.join(dir_path, 'common.yaml'), 'w', encoding='utf-8') as stream:
        dump(common, stream, Dumper=Dumper, allow_unicode=True)


def save_zones(zones):
    config['zones'] = zones
    with open(os.path.join(dir_path, 'zones.yaml'), 'w', encoding='utf-8') as stream:
        dump(zones, stream, Dumper=Dumper, allow_unicode=True)
